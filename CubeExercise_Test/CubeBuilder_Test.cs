using CubeExercise.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeExercise_Test
{
    [TestClass]
    public class CubeBuilder_Test
    {

        [TestMethod]
        public void CubesDoNotIntersect()
        {
            var cube1 = new CubeBuilder().SetCubeValues(2, 2, 2, 2).CreateCube();
            var cube2 = new CubeBuilder().SetCubeValues(30, 30, 30, 5).CreateCube();

            Assert.AreEqual(0, cube1.IntersectionVolume(cube2));
        }

        [TestMethod]
        public void CubesWithSameWithAndHeight()
        {
            var cube1 = new CubeBuilder().SetCubeValues(2, 2, 2, 2).CreateCube();
            var cube2 = new CubeBuilder().SetCubeValues(2, 2, 1, 2).CreateCube();

            Assert.AreEqual(4, cube1.IntersectionVolume(cube2));
        }

        [TestMethod]
        public void CubesWithSameDepthAndHeight()
        {
            var cube1 = new CubeBuilder().SetCubeValues(2, 2, 2, 2).CreateCube();
            var cube2 = new CubeBuilder().SetCubeValues(1, 2, 2, 5).CreateCube();

            Assert.AreEqual(8, cube1.IntersectionVolume(cube2));
        }

        [TestMethod]
        public void CubesWithSameDepthAndWith()
        {
            var cube1 = new CubeBuilder().SetCubeValues(2, 2, 2, 2).CreateCube();
            var cube2 = new CubeBuilder().SetCubeValues(2, 3, 2, 2).CreateCube();

            Assert.AreEqual(4, cube1.IntersectionVolume(cube2));
        }
    }
}
