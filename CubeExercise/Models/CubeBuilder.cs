﻿using System.Reflection.Metadata.Ecma335;

namespace CubeExercise.Models
{
    public class CubeBuilder
    {
        private Coordinate center;
        private double lenght;
        public CubeBuilder SetCubeValues(double x, double y, double z, double _lenght)
        {
            center = new Coordinate(x, y, z);
            lenght = _lenght;
            return this;
        }
        public Cube CreateCube()
        {
            return new Cube(center, lenght);
        } 

    }
}
