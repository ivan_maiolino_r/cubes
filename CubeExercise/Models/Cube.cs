﻿namespace CubeExercise.Models
{
    public class Cube
    {
        private Edge height;
        private Edge width;
        private Edge depth;

        public Cube(Coordinate center, double lenght)
        {
            height = new Edge(center.Y, lenght);
            width = new Edge(center.X, lenght);
            depth = new Edge(center.Z, lenght);
        }

        public double IntersectionVolume(Cube cube)
        {
             return width.Overlap(cube.width) * height.Overlap(cube.height) * depth.Overlap(cube.depth);
        }
    }
}
